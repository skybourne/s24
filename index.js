const cube = Math.pow(2, 3);
console.log(`The cube of 2 is ${cube}`);

let Address = ["258", "Washington Ave NW", "California", "90011"]

let [houseNumber, street, state, zipCode] = Address;

console.log(`I live at ${houseNumber} ${street}, ${state} ${zipCode}`);

class Animal{
	constructor(name, specie, weight, measurement){
		this.name = name;
		this.specie = specie;
		this.weight = weight;
		this.measurement = measurement;
	}
}

animal = new Animal("Lolong", "saltwater crocodile", "1075kgs", "20ft 3 in");

console.log(`${animal.name} was a ${animal.specie}. He weighed at ${animal.weight} with a measurement of ${animal.measurement}.`)

let numbers = [1,2,3,4,5];

numbers.forEach((number) =>{
	console.log(number);
});

let reduceNumber = numbers.reduce((accumulator, currentValue) => {
	return accumulator + currentValue;
});

console.log(reduceNumber);

class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
		
	}
}

let myDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(myDog);